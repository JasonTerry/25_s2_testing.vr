comment "25th MEU Pilot";

private ["_unit"];
_unit = _this select 0;
IF(!local _unit) exitwith {};

comment "CLEAR";
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;

comment "UNIFORM";
_unit forceAddUniform "CUP_U_B_USMC_PilotOverall";
_unit addHeadgear "rhsusf_hgu56p_mask";
_unit addItemToUniform "ACE_EarPlugs";
_unit addItemToUniform "ACE_Flashlight_MX991";
for "_i" from 1 to 8 do {_unit addItemToUniform "ACE_elasticBandage";};
for "_i" from 1 to 8 do {_unit addItemToUniform "ACE_quikclot";};
for "_i" from 1 to 4 do {_unit addItemToUniform "ACE_tourniquet";};
for "_i" from 1 to 8 do {_unit addItemToUniform "ACE_packingBandage";};
for "_i" from 1 to 4 do {_unit addItemToUniform "ACE_CableTie";};

comment "VEST";
_unit addVest "CUP_V_B_PilotVest";
for "_i" from 1 to 4 do {_unit addItemToVest "rhsusf_mag_15Rnd_9x19_JHP";};
for "_i" from 1 to 2 do {_unit addItemToVest "1Rnd_SmokeGreen_Grenade_shell";};
for "_i" from 1 to 2 do {_unit addItemToVest "1Rnd_SmokeYellow_Grenade_shell";};
for "_i" from 1 to 2 do {_unit addItemToVest "Chemlight_green";};
for "_i" from 1 to 2 do {_unit addItemToVest "Chemlight_red";};
for "_i" from 1 to 2 do {_unit addItemToVest "rhs_mag_m661_green";};
for "_i" from 1 to 2 do {_unit addItemToVest "UGL_FlareRed_F";};
for "_i" from 1 to 2 do {_unit addItemToVest "UGL_FlareYellow_F";};
for "_i" from 1 to 2 do {_unit addItemToVest "UGL_FlareGreen_F";};

comment "BACKPACK";
_unit addBackpack "tf_rt1523g_sage";
_unit addItemToBackpack "ToolKit";
_unit addItemToBackpack "rhs_weap_M320";
_unit addItemToBackpack "B_UavTerminal";

comment "WEAPONS";
_unit addWeapon "rhsusf_weap_m9";

comment "ITEMS";
_unit linkItem "ItemMap";
_unit linkItem "ItemCompass";
_unit linkItem "ACE_Altimeter";
_unit linkItem "tf_anprc152_4";
_unit linkItem "ItemcTab";
_unit linkItem "rhsusf_ANPVS_15";
