comment "25th MEU Scout Sniper M107 M81";

private ["_unit"];
_unit = _this select 0;
IF(!local _unit) exitwith {};

comment "CLEAR";
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;

comment "UNIFORM";
// ADD UNIFORM M81
// ADD HEADGEAR
_unit addItemToUniform "ACE_Flashlight_MX991";
_unit addItemToUniform "ACE_MapTools";
_unit addItemToUniform "ACE_RangeCard";
for "_i" from 1 to 8 do {_unit addItemToUniform "ACE_elasticBandage";};
for "_i" from 1 to 8 do {_unit addItemToUniform "ACE_quikclot";};
_unit addItemToUniform "ACE_EarPlugs";
for "_i" from 1 to 4 do {_unit addItemToUniform "ACE_tourniquet";};
for "_i" from 1 to 8 do {_unit addItemToUniform "ACE_packingBandage";};
for "_i" from 1 to 2 do {_unit addItemToUniform "ACE_CableTie";};
for "_i" from 1 to 2 do {_unit addItemToUniform "ACE_morphine";};
for "_i" from 1 to 2 do {_unit addItemToUniform "ACE_epinephrine";};

comment "VEST";
_unit addVest "rhsusf_spc_light";
_unit addItemToVest "ACE_ATragMX";
_unit addItemToVest "ACE_Kestrel4500";
_unit addItemToVest "ACE_microDAGR";
for "_i" from 1 to 3 do {_unit addItemToVest "rhsusf_mag_15Rnd_9x19_FMJ";};
for "_i" from 1 to 5 do {_unit addItemToVest "CUP_10Rnd_127x99_M107";};

comment "BACKPACK";
_unit addBackpack "rhsusf_assault_eagleaiii_coy";
_unit addItemToBackpack "ACE_Tripod";
_unit addItemToBackpack "ACE_EntrenchingTool";
_unit addItemToBackpack "ACE_IR_Strobe_Item";
_unit addItemToBackpack "CUP_optic_AN_PVS_10";
for "_i" from 1 to 10 do {_unit addItemToBackpack "CUP_5Rnd_762x51_M24";};
for "_i" from 1 to 2 do {_unit addItemToBackpack "Chemlight_green";};
for "_i" from 1 to 2 do {_unit addItemToBackpack "Chemlight_red";};
for "_i" from 1 to 2 do {_unit addItemToBackpack "rhs_mag_an_m8hc";};
for "_i" from 1 to 2 do {_unit addItemToBackpack "rhs_mag_m18_red";};
for "_i" from 1 to 2 do {_unit addItemToBackpack "rhs_mag_m67";};

comment "WEAPONS";
_unit addWeapon "CUP_srifle_M107_Base";
_unit addPrimaryWeaponItem "optic_LRPS";
_unit addWeapon "rhsusf_weap_m9";
_unit addWeapon "ACE_Vector";

comment "ITEMS";
_unit linkItem "ItemMap";
_unit linkItem "ItemCompass";
_unit linkItem "ACE_Altimeter";
_unit linkItem "tf_anprc152_12";
_unit linkItem "ItemcTab";
_unit linkItem "CUP_NVG_HMNVS";
