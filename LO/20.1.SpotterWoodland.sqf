comment "25th MEU Scout Spotter Woodland";

private ["_unit"];
_unit = _this select 0;
IF(!local _unit) exitwith {};

comment "CLEAR";
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;

comment "UNIFORM";
_unit forceAddUniform "rhs_uniform_FROG01_wd";
_unit addHeadgear "rhsusf_mich_helmet_marpatwd_norotos_headset";
_unit addItemToUniform "ACE_Flashlight_MX991";
_unit addItemToUniform "ACE_MapTools";
_unit addItemToUniform "ACE_RangeCard";
_unit addItemToUniform "ACE_EarPlugs";
for "_i" from 1 to 8 do {_unit addItemToUniform "ACE_elasticBandage";};
for "_i" from 1 to 8 do {_unit addItemToUniform "ACE_quikclot";};
for "_i" from 1 to 4 do {_unit addItemToUniform "ACE_tourniquet";};
for "_i" from 1 to 8 do {_unit addItemToUniform "ACE_packingBandage";};
for "_i" from 1 to 2 do {_unit addItemToUniform "ACE_CableTie";};
for "_i" from 1 to 2 do {_unit addItemToUniform "ACE_morphine";};
for "_i" from 1 to 2 do {_unit addItemToUniform "ACE_epinephrine";};

comment "VEST";
_unit addVest "rhsusf_spc_light";
_unit addItemToVest "ACE_microDAGR";
_unit addItemToVest "ACE_RangeCard";
_unit addItemToVest "ACE_Kestrel4500";
_unit addItemToVest "ACE_ATragMX";
for "_i" from 1 to 5 do {_unit addItemToVest "rhs_mag_30Rnd_556x45_Mk318_Stanag";};
for "_i" from 1 to 4 do {_unit addItemToVest "rhs_mag_M441_HE";};
for "_i" from 1 to 2 do {_unit addItemToVest "rhs_mag_m713_Red";};
for "_i" from 1 to 2 do {_unit addItemToVest "rhs_mag_m715_Green";};
for "_i" from 1 to 2 do {_unit addItemToVest "rhs_mag_m714_White";};
for "_i" from 1 to 2 do {_unit addItemToVest "rhsusf_mag_15Rnd_9x19_JHP";};

comment "BACKPACK";
_unit addBackpack "tf_rt1523g_sage";
_unit addItemToBackpack "CUP_optic_AN_PVS_10";
for "_i" from 1 to 2 do {_unit addItemToBackpack "Chemlight_green";};
for "_i" from 1 to 2 do {_unit addItemToBackpack "Chemlight_red";};
for "_i" from 1 to 2 do {_unit addItemToBackpack "rhs_mag_an_m8hc";};
for "_i" from 1 to 2 do {_unit addItemToBackpack "rhs_mag_m18_red";};
for "_i" from 1 to 2 do {_unit addItemToBackpack "rhs_mag_m67";};
for "_i" from 1 to 4 do {_unit addItemToBackpack "rhs_mag_30Rnd_556x45_Mk318_Stanag";};
for "_i" from 1 to 2 do {_unit addItemToBackpack "rhsusf_mag_15Rnd_9x19_JHP";};

comment "WEAPONS";
_unit addWeapon "rhs_weap_m16a4_carryhandle_M203";
_unit addPrimaryWeaponItem "rhsusf_acc_anpeq15";
_unit addPrimaryWeaponItem "rhsusf_acc_ACOG_d";
_unit addWeapon "rhsusf_weap_m9";
_unit addWeapon "ACE_Vector";

comment "ITEMS";
_unit linkItem "ItemMap";
_unit linkItem "ItemCompass";
_unit linkItem "ACE_Altimeter";
_unit linkItem "tf_anprc152_10";
_unit linkItem "ItemcTab";
_unit linkItem "CUP_NVG_HMNVS";
