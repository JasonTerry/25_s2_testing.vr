comment "25thMEU Assaultmen Gear - vehicle Cargo Script";
// Call this script with
// null = [this] execVM "LO\23.AssaultmanGear.sqf"
private ["_veh"];
_veh = _this select 0;
// _veh = HMMWV TOW;

comment "CLEAR";
clearBackpackCargoGlobal _veh;
clearItemCargoGlobal _veh;
clearWeaponCargoGlobal _veh;
clearMagazineCargoGlobal _veh;

comment "MAGAZINES CARGO";
_veh addMagazineCargoGlobal ["rhs_mag_30Rnd_556x45_Mk318_Stanag", 15];
_veh addMagazineCargoGlobal ["rhsusf_mag_15Rnd_9x19_FMJ", 4];
_veh addMagazineCargoGlobal ["rhs_mag_m67", 6];
_veh addMagazineCargoGlobal ["rhs_mag_an_m8hc", 6];
_veh addMagazineCargoGlobal ["rhs_mag_m18_red", 6];
_veh addMagazineCargoGlobal ["SmokeShellGreen", 6];
_veh addMagazineCargoGlobal ["CUP_SMAW_HEAA_M", 12];
_veh addMagazineCargoGlobal ["CUP_SMAW_HEDP_M", 12];
_veh addMagazineCargoGlobal ["CUP_SMAW_Spotting", 12];

comment "BACKPACK CARGO";
_veh addBackpackCargoGlobal ["tf_rt1523g_sage", 2];

comment "WEAPONS CARGO";
_veh addWeaponCargoGlobal ["CUP_launch_Mk153Mod0", 2];

comment "ITEMS CARGO";
_veh addItemCargoGlobal ["ACE_EarPlugs", 2];
_veh addItemCargoGlobal ["ACE_microDAGR", 2];
_veh addItemCargoGlobal ["ACE_IR_Strobe_Item", 4];
_veh addItemCargoGlobal ["CUP_optic_SMAW_Scope", 2];
_veh addItemCargoGlobal ["ACE_EntrenchingTool", 4];

comment "FIRST AID";
_veh addItemCargoGlobal ["ACE_quikclot", 48];
_veh addItemCargoGlobal ["ACE_tourniquet", 8];
_veh addItemCargoGlobal ["ACE_salineIV_500", 16];
_veh addItemCargoGlobal ["ACE_packingBandage", 48];
_veh addItemCargoGlobal ["ACE_elasticBandage", 48];
_veh addItemCargoGlobal ["ACE_morphine". 10];
_veh addItemCargoGlobal ["ACE_epinephrine". 10];
