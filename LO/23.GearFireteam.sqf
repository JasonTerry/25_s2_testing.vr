comment "25thMEU Rifle Fire Team Gear - vehicle Cargo Script";
// Call this script with
// null = [this] execVM "LO\23.GearFireteam.sqf"
private ["_veh"];
_veh = _this select 0;
// _veh = HMMWV M240B or M2;

comment "CLEAR";
clearBackpackCargoGlobal _veh;
clearItemCargoGlobal _veh;
clearWeaponCargoGlobal _veh;
clearMagazineCargoGlobal _veh;

comment "MAGAZINES CARGO";
_veh addMagazineCargoGlobal ["rhs_mag_30Rnd_556x45_Mk318_Stanag", 90];
_veh addMagazineCargoGlobal ["rhsusf_mag_15Rnd_9x19_FMJ", 8];
_veh addMagazineCargoGlobal ["rhs_mag_m67", 8];
_veh addMagazineCargoGlobal ["rhs_mag_an_m8hc", 16];
_veh addMagazineCargoGlobal ["rhs_mag_m18_red", 12];
_veh addMagazineCargoGlobal ["SmokeShellGreen", 12];
_veh addMagazineCargoGlobal ["rhs_mag_M433_HEDP", 10];
_veh addMagazineCargoGlobal ["rhs_mag_m662_red", 3];
_veh addMagazineCargoGlobal ["rhs_mag_m661_green", 3];
_veh addMagazineCargoGlobal ["rhs_mag_m714_White", 6];
_veh addMagazineCargoGlobal ["rhs_mag_m713_Red", 6];

comment "BACKPACK CARGO";
_veh addBackpackCargoGlobal ["tf_rt1523g_sage",1];
_veh addBackpackCargoGlobal ["rhsusf_assault_eagleaiii_coy", 3];

comment "WEAPONS CARGO";
_veh addWeaponCargoGlobal ["rhs_weap_M136", 4];

comment "ITEMS CARGO";
_veh addItemCargoGlobal ["ACE_EarPlugs", 4];
_veh addItemCargoGlobal ["ACE_Flashlight_MX991", 4];
_veh addItemCargoGlobal ["ACE_EntrenchingTool", 4];
_veh addItemCargoGlobal ["ACE_CableTie", 16];
_veh addItemCargoGlobal ["Chemlight_green", 8];
_veh addItemCargoGlobal ["ItemAndroid", 4];
_veh addWeaponCargoGlobal ["ACE_Vector",1];

comment "FIRST AID";
_veh addItemCargoGlobal ["ACE_quikclot",48];
_veh addItemCargoGlobal ["ACE_tourniquet",16];
_veh addItemCargoGlobal ["ACE_salineIV_500",16];
_veh addItemCargoGlobal ["ACE_packingBandage",48];
_veh addItemCargoGlobal ["ACE_elasticBandage",48];
