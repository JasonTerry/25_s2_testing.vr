comment "25thMEU MEV Gear - vehicle Cargo Script";
// Call this script with
// null = [this] execVM "LO\23.MEVGear.sqf"
private ["_veh"];
_veh = _this select 0;
// _veh = All MEV Vics;

comment "CLEAR";
clearBackpackCargoGlobal _veh;
clearItemCargoGlobal _veh;
clearWeaponCargoGlobal _veh;
clearMagazineCargoGlobal _veh;

comment "MAGAZINES CARGO";
_veh addMagazineCargoGlobal ["rhs_mag_30Rnd_556x45_Mk318_Stanag", 90];
_veh addMagazineCargoGlobal ["rhsusf_mag_15Rnd_9x19_FMJ", 8];
_veh addMagazineCargoGlobal ["rhs_mag_m67", 8];
_veh addMagazineCargoGlobal ["rhs_mag_an_m8hc", 16];
_veh addMagazineCargoGlobal ["rhs_mag_m18_red", 12];
_veh addMagazineCargoGlobal ["SmokeShellGreen", 12];

comment "BACKPACK CARGO";

comment "WEAPONS CARGO";

comment "ITEMS CARGO";
_veh addItemCargoGlobal ["ACE_EarPlugs", 4];
_veh addItemCargoGlobal ["ACE_Flashlight_MX991", 4];
_veh addItemCargoGlobal ["ACE_EntrenchingTool", 4];
_veh addItemCargoGlobal ["ACE_CableTie", 16];
_veh addItemCargoGlobal ["Chemlight_green", 8];
_veh addItemCargoGlobal ["ItemAndroid", 4];
_veh addItemCargoGlobal ["ACE_Vector", 4];
_veh addItemCargoGlobal ["ACE_Altimeter", 1];
_veh addItemCargoGlobal ["ACE_microDAGR", 4];

comment "FIRST AID";
_veh addItemCargoGlobal ["ACE_quikclot", 96];
_veh addItemCargoGlobal ["ACE_tourniquet", 48];
_veh addItemCargoGlobal ["ACE_salineIV_500", 12];
_veh addItemCargoGlobal ["ACE_packingBandage", 96];
_veh addItemCargoGlobal ["ACE_elasticBandage", 96];
_veh addItemCargoGlobal ["ACE_morphine", 24];
_veh addItemCargoGlobal ["ACE_epinephrine", 24];
