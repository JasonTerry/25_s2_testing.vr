comment "25thMEU Rifle Fire Team Gear - vehicle Cargo Script";
// Call this script with
// null = [this] execVM "LO\23.GearFireteam.sqf"
private ["_veh"];
_veh = _this select 0;
// _veh = HMMWV M240B or M2;

comment "CLEAR";
clearBackpackCargoGlobal _veh;
clearItemCargoGlobal _veh;
clearWeaponCargoGlobal _veh;
clearMagazineCargoGlobal _veh;

comment "MAGAZINES CARGO";
_veh addMagazineCargoGlobal ["rhs_mag_30Rnd_556x45_Mk318_Stanag", 270];
_veh addMagazineCargoGlobal ["rhsusf_mag_15Rnd_9x19_FMJ", 24];
_veh addMagazineCargoGlobal ["rhs_mag_m67", 24];
_veh addMagazineCargoGlobal ["rhs_mag_an_m8hc", 64];
_veh addMagazineCargoGlobal ["rhs_mag_m18_red", 48];
_veh addMagazineCargoGlobal ["SmokeShellGreen", 48];
_veh addMagazineCargoGlobal ["rhs_mag_M433_HEDP", 40];
_veh addMagazineCargoGlobal ["rhs_mag_m662_red", 12];
_veh addMagazineCargoGlobal ["rhs_mag_m661_green", 12];
_veh addMagazineCargoGlobal ["rhs_mag_m714_White", 24];
_veh addMagazineCargoGlobal ["rhs_mag_m713_Red", 24];

comment "BACKPACK CARGO";
_veh addBackpackCargoGlobal ["tf_rt1523g_sage", 4];
_veh addBackpackCargoGlobal ["rhsusf_assault_eagleaiii_coy", 12];

comment "WEAPONS CARGO";
_veh addWeaponCargoGlobal ["rhs_weap_M136", 12];

comment "ITEMS CARGO";
_veh addItemCargoGlobal ["ACE_EarPlugs", 12];
_veh addItemCargoGlobal ["ACE_Flashlight_MX991", 12];
_veh addItemCargoGlobal ["ACE_EntrenchingTool", 12];
_veh addItemCargoGlobal ["ACE_CableTie", 64];
_veh addItemCargoGlobal ["Chemlight_green", 24];
_veh addItemCargoGlobal ["ItemAndroid", 16];
_veh addWeaponCargoGlobal ["ACE_Vector", 4];

comment "FIRST AID";
_veh addItemCargoGlobal ["ACE_quikclot", 192];
_veh addItemCargoGlobal ["ACE_tourniquet", 64];
_veh addItemCargoGlobal ["ACE_salineIV_500", 64];
_veh addItemCargoGlobal ["ACE_packingBandage", 192];
_veh addItemCargoGlobal ["ACE_elasticBandage", 192];
