comment "25thMEU Machineguns Gear - vehicle Cargo Script";
// Call this script with
// null = [this] execVM "LO\23.MachinegunsGear.sqf"
private ["_veh"];
_veh = _this select 0;
// _veh = HMMWV M240B or M2;

comment "CLEAR";
clearBackpackCargoGlobal _veh;
clearItemCargoGlobal _veh;
clearWeaponCargoGlobal _veh;
clearMagazineCargoGlobal _veh;

comment "MAGAZINES CARGO";
_veh addMagazineCargoGlobal ["rhs_mag_30Rnd_556x45_Mk318_Stanag", 15];
_veh addMagazineCargoGlobal ["rhsusf_mag_15Rnd_9x19_FMJ", 4];
_veh addMagazineCargoGlobal ["rhs_mag_m67", 6];
_veh addMagazineCargoGlobal ["rhs_mag_an_m8hc", 6];
_veh addMagazineCargoGlobal ["rhs_mag_m18_red", 6];
_veh addMagazineCargoGlobal ["SmokeShellGreen", 6];
_veh addMagazineCargoGlobal ["rhsusf_100Rnd_762x51", 10];
_veh addMagazineCargoGlobal ["rhsusf_100Rnd_762x51_m61_ap", 10];

comment "BACKPACK CARGO";
_veh addBackpackCargoGlobal ["tf_rt1523g_sage", 2];
_veh addBackpackCargoGlobal ["rhsusf_assault_eagleaiii_coy", 2];

comment "ITEMS CARGO";
_veh addItemCargoGlobal ["ACE_EarPlugs", 2];
_veh addItemCargoGlobal ["ACE_IR_Strobe_Item", 4];
_veh addItemCargoGlobal ["ACE_EntrenchingTool", 2];
_veh addItemCargoGlobal ["ACE_SpareBarrel", 2];
_veh addItemCargoGlobal ["rhsusf_acc_ELCAN_ard", 2];

comment "WEAPONS CARGO";
_veh addWeaponCargoGlobal ["rhs_weap_m240B", 2];

comment "FIRST AID";
_veh addItemCargoGlobal ["ACE_quikclot", 48];
_veh addItemCargoGlobal ["ACE_tourniquet", 8];
_veh addItemCargoGlobal ["ACE_salineIV_500", 16];
_veh addItemCargoGlobal ["ACE_packingBandage", 48];
_veh addItemCargoGlobal ["ACE_elasticBandage", 48];
_veh addItemCargoGlobal ["ACE_morphine", 10];
_veh addItemCargoGlobal ["ACE_epinephrine", 10];
