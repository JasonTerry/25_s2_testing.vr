comment "25thMEU Mortar Gear - vehicle Cargo Script";
// Call this script with:
// null = [this] execVM "LO\23.MortarGear.sqf"
private ["_veh"];
_veh = _this select 0;
// _veh = HMMWV M240B;

comment "CLEAR";
clearBackpackCargoGlobal _veh;
clearItemCargoGlobal _veh;
clearWeaponCargoGlobal _veh;
clearMagazineCargoGlobal _veh;

comment "MAGAZINES CARGO";
_veh addMagazineCargoGlobal ["ACE_1Rnd_82mm_Mo_He", 60];
_veh addMagazineCargoGlobal ["ACE_1Rnd_82mm_Mo_Smoke", 30];
_veh addMagazineCargoGlobal ["ACE_1Rnd_82mm_Mo_Illum", 30];

comment "BACKPACK CARGO";
_veh addBackpackCargoGlobal ["tf_rt1523g_sage",1];
_veh addBackpackCargoGlobal ["0_Mortar_01_support_F", 3];

comment "WEAPONS CARGO";
_veh addBackpackCargoGlobal ["O_Mortar_01_weapon_F", 3];

comment "ITEMS CARGO";
_veh addItemCargoGlobal ["ACE_microDAGR", 2];
_veh addItemCargoGlobal ["ACE_Vector", 1];
_veh addItemCargoGlobal ["ACE_RangeTable_82mm", 1];

comment "FIRST AID";
_veh addItemCargoGlobal ["ACE_quikclot",48];
_veh addItemCargoGlobal ["ACE_tourniquet",16];
_veh addItemCargoGlobal ["ACE_salineIV_500",16];
_veh addItemCargoGlobal ["ACE_packingBandage",48];
_veh addItemCargoGlobal ["ACE_elasticBandage",48];
