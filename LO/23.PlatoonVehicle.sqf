comment "25thMEU Platoon Vehicle Gear (CH53-E) - vehicle Cargo Script";
// Call this script with null = [this] execVM "LO\23.PlatoonVehicle.sqf"
private ["_veh"];
_veh = _this select 0;
// _veh = CH53-E Super Stallion;

comment "CLEAR";
clearBackpackCargoGlobal _veh;
clearItemCargoGlobal _veh;
clearWeaponCargoGlobal _veh;
clearMagazineCargoGlobal _veh;

comment "MAGAZINES CARGO";
_veh addMagazineCargoGlobal ["rhs_mag_30Rnd_556x45_Mk318_Stanag", 180];
_veh addMagazineCargoGlobal ["rhsusf_mag_15Rnd_9x19_FMJ", 16];
_veh addMagazineCargoGlobal ["rhs_mag_m67", 16];
_veh addMagazineCargoGlobal ["rhs_mag_an_m8hc", 32];
_veh addMagazineCargoGlobal ["rhs_mag_m18_red", 24];
_veh addMagazineCargoGlobal ["SmokeShellGreen", 24];
_veh addMagazineCargoGlobal ["rhs_mag_M433_HEDP", 20];
_veh addMagazineCargoGlobal ["rhs_mag_m662_red", 6];
_veh addMagazineCargoGlobal ["rhs_mag_m661_green", 6];
_veh addMagazineCargoGlobal ["rhs_mag_m714_White", 12];
_veh addMagazineCargoGlobal ["rhs_mag_m713_Red", 12];
_veh addMagazineCargoGlobal ["CUP_SMAW_HEAA_M", 24];
_veh addMagazineCargoGlobal ["CUP_SMAW_HEDP_M", 24];
_veh addMagazineCargoGlobal ["CUP_SMAW_Spotting", 24];
_veh addMagazineCargoGlobal ["rhsusf_100Rnd_762x51", 40];
_veh addMagazineCargoGlobal ["rhsusf_100Rnd_762x51_m61_ap", 20];
_veh addMagazineCargoGlobal ["ACE_1Rnd_82mm_Mo_He", 30];
_veh addMagazineCargoGlobal ["ACE_1Rnd_82mm_Mo_Smoke", 15];
_veh addMagazineCargoGlobal ["ACE_1Rnd_82mm_Mo_Illum", 15];

comment "BACKPACK CARGO";
_veh addBackpackCargoGlobal ["tf_rt1523g_sage", 2];
_veh addBackpackCargoGlobal ["rhsusf_assault_eagleaiii_coy", 4];
_veh addBackpackCargoGlobal ["O_Mortar_01_weapon_F", 6];

comment "WEAPONS CARGO";
_veh addWeaponCargoGlobal ["rhs_weap_M136", 6];

comment "ITEMS CARGO";
_veh addItemCargoGlobal ["ACE_EarPlugs", 8];
_veh addItemCargoGlobal ["ACE_Flashlight_MX991", 8];
_veh addItemCargoGlobal ["ACE_EntrenchingTool", 8];
_veh addItemCargoGlobal ["ACE_CableTie", 32];
_veh addItemCargoGlobal ["Chemlight_green", 16];
_veh addItemCargoGlobal ["ItemAndroid", 8];
_veh addItemCargoGlobal ["ACE_Vector", 8];
_veh addItemCargoGlobal ["ACE_microDAGR", 8];
_veh addBackpackCargoGlobal ["0_Mortar_01_support_F", 3];
_veh addItemCargoGlobal ["ACE_RangeTable_82mm", 1];

comment "FIRST AID";
_veh addItemCargoGlobal ["ACE_quikclot", 64];
_veh addItemCargoGlobal ["ACE_tourniquet", 64];
_veh addItemCargoGlobal ["ACE_salineIV_500", 64];
_veh addItemCargoGlobal ["ACE_packingBandage", 192];
_veh addItemCargoGlobal ["ACE_elasticBandage", 192];
