comment "25thMEU Rotary Vehicle Gear - vehicle Cargo Script";
// Call this script with
// null = [this] execVM "LO\23.RotaryVehicle.sqf"
private ["_veh"];
_veh = _this select 0;
// _veh = All Transport Helicopters EXCEPT MEV Venom;

comment "CLEAR";
clearBackpackCargoGlobal _veh;
clearItemCargoGlobal _veh;
clearWeaponCargoGlobal _veh;
clearMagazineCargoGlobal _veh;

comment "MAGAZINES CARGO";
_veh addMagazineCargoGlobal ["rhs_mag_30Rnd_556x45_Mk318_Stanag", 90];
_veh addMagazineCargoGlobal ["rhsusf_mag_15Rnd_9x19_FMJ", 8];
_veh addMagazineCargoGlobal ["rhs_mag_m67", 8];
_veh addMagazineCargoGlobal ["rhs_mag_an_m8hc", 16];
_veh addMagazineCargoGlobal ["rhs_mag_m18_red", 12];
_veh addMagazineCargoGlobal ["SmokeShellGreen", 12];
_veh addMagazineCargoGlobal ["rhs_mag_M433_HEDP", 10];
_veh addMagazineCargoGlobal ["rhs_mag_m662_red", 3];
_veh addMagazineCargoGlobal ["rhs_mag_m661_green", 3];
_veh addMagazineCargoGlobal ["rhs_mag_m714_White", 6];
_veh addMagazineCargoGlobal ["rhs_mag_m713_Red", 6];
_veh addMagazineCargoGlobal ["CUP_SMAW_HEAA_M", 12];
_veh addMagazineCargoGlobal ["CUP_SMAW_HEDP_M", 12];
_veh addMagazineCargoGlobal ["CUP_SMAW_Spotting", 12];
_veh addMagazineCargoGlobal ["rhsusf_100Rnd_762x51", 20];
_veh addMagazineCargoGlobal ["rhsusf_100Rnd_762x51_m61_ap", 10];
_veh addMagazineCargoGlobal ["ACE_Box_82mm_Mo_He", 1];
_veh addMagazineCargoGlobal ["ACE_Box_82mm_Mo_Smoke", 1];
_veh addMagazineCargoGlobal ["ACE_Box_82mm_Mo_Illum", 1];

comment "BACKPACK CARGO";
_veh addBackpackCargoGlobal ["tf_rt1523g_sage",1];
_veh addBackpackCargoGlobal ["rhsusf_assault_eagleaiii_coy", 3];
_veh addBackpackCargoGlobal ["O_Mortar_01_weapon_F", 3];
_veh addBackpackCargoGlobal ["0_Mortar_01_support_F", 3];

comment "WEAPONS CARGO";
_veh addWeaponCargoGlobal ["rhs_weap_M136", 4];

comment "ITEMS CARGO";
_veh addItemCargoGlobal ["ACE_EarPlugs", 4];
_veh addItemCargoGlobal ["ACE_Flashlight_MX991", 4];
_veh addItemCargoGlobal ["ACE_EntrenchingTool", 4];
_veh addItemCargoGlobal ["ACE_CableTie", 16];
_veh addItemCargoGlobal ["Chemlight_green", 8];
_veh addItemCargoGlobal ["ItemAndroid", 4];
_veh addItemCargoGlobal ["ACE_Vector", 4];
_veh addItemCargoGlobal ["ACE_Altimeter", 1];
_veh addItemCargoGlobal ["ACE_microDAGR", 4];

comment "FIRST AID";
_veh addItemCargoGlobal ["ACE_quikclot", 32];
_veh addItemCargoGlobal ["ACE_tourniquet", 32];
_veh addItemCargoGlobal ["ACE_salineIV_500", 32];
_veh addItemCargoGlobal ["ACE_packingBandage", 96];
_veh addItemCargoGlobal ["ACE_elasticBandage", 96];
