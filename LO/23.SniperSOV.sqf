comment "25thMEU Sniper SOV Gear - vehicle Cargo Script";
// Call this script with
// null = [this] execVM "LO\23.SniperSOV.sqf"
private ["_veh"];
_veh = _this select 0;
// _veh = HMMWV SOV of both variants;

comment "CLEAR";
clearBackpackCargoGlobal _veh;
clearItemCargoGlobal _veh;
clearWeaponCargoGlobal _veh;
clearMagazineCargoGlobal _veh;

comment "MAGAZINES CARGO";
_veh addMagazineCargoGlobal ["rhs_mag_30Rnd_556x45_Mk318_Stanag", 10];
_veh addMagazineCargoGlobal ["rhsusf_mag_15Rnd_9x19_FMJ", 4];
_veh addMagazineCargoGlobal ["rhs_mag_m67", 4];
_veh addMagazineCargoGlobal ["rhs_mag_an_m8hc", 8];
_veh addMagazineCargoGlobal ["rhs_mag_m18_red", 6];
_veh addMagazineCargoGlobal ["SmokeShellGreen", 6];
_veh addMagazineCargoGlobal ["CUP_10Rnd_127x99_M107", 5];
_veh addMagazineCargoGlobal ["rhs_mag_M441_HE", 4];
_veh addMagazineCargoGlobal ["rhs_mag_m713_Red", 2];
_veh addMagazineCargoGlobal ["rhs_mag_m715_Green", 2];
_veh addMagazineCargoGlobal ["rhs_mag_m714_White", 2];
_veh addMagazineCargoGlobal ["CUP_5Rnd_762x51_M24", 10];
_veh addMagazineCargoGlobal ["CUP_20Rnd_762x51_B_M100", 4];

comment "BACKPACK CARGO";
_veh addBackpackCargoGlobal ["tf_rt1523g_sage", 2];

comment "WEAPONS CARGO";
_veh addWeaponCargoGlobal ["CUP_srifle_M107_Base", 1];
_veh addWeaponCargoGlobal ["CUP_srifle_M110", 1];
_veh addWeaponCargoGlobal ["CUP_srifle_M40A3", 1];
_veh addWeaponCargoGlobal ["rhsusf_weap_m9", 2];
_veh addWeaponCargoGlobal ["rhs_weap_m16a4_carryhandle_M203", 1];
_veh addWeaponCargoGlobal ["ACE_Vector", 2];

comment "ITEMS CARGO";
_veh addItemCargoGlobal ["rhsusf_mich_helmet_marpatd_norotos_arc_headset", 2];
_veh addItemCargoGlobal ["ACE_EarPlugs", 2];
_veh addItemCargoGlobal ["optic_LRPS", 3];
_veh addItemCargoGlobal ["ACE_Altimeter", 1];
_veh addItemCargoGlobal ["rhsusf_acc_ACOG_d", 1];
_veh addItemCargoGlobal ["rhsusf_acc_anpeq15", 2];
_veh addItemCargoGlobal ["ACE_microDAGR", 2];
_veh addItemCargoGlobal ["ACE_RangeCard", 2];
_veh addItemCargoGlobal ["ACE_Kestrel4500", 2];
_veh addItemCargoGlobal ["ACE_ATragMX", 2];
_veh addItemCargoGlobal ["CUP_optic_AN_PVS_10", 3];
_veh addItemCargoGlobal ["CUP_bipod_Harris_1A2_L", 2];
_veh addItemCargoGlobal ["ACE_IR_Strobe_Item", 4];
_veh addItemCargoGlobal ["ACE_EntrenchingTool", 2];

comment "FIRST AID";
_veh addItemCargoGlobal ["ACE_quikclot", 48];
_veh addItemCargoGlobal ["ACE_tourniquet", 8];
_veh addItemCargoGlobal ["ACE_salineIV_500", 16];
_veh addItemCargoGlobal ["ACE_packingBandage", 48];
_veh addItemCargoGlobal ["ACE_elasticBandage", 48];
<<<<<<< HEAD
_veh addItemCargoGlobal ["ACE_morphine", 10];
_veh addItemCargoGlobal ["ACE_epinephrine", 10];
=======
_veh addItemCargoGlobal ["ACE_morphine". 10];
_veh addItemCargoGlobal ["ACE_epinephrine". 10];
>>>>>>> 9746784d0aafae301d1f04567e41868490fd9a8a
