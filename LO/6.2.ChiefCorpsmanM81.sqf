comment "25th MEU Corpsman M81";

private ["_unit"];
_unit = _this select 0;
IF(!local _unit) exitwith {};

comment "CLEAR";
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;

comment "UNIFORM";
_unit forceAddUniform "rhs_uniform_FROG01_m81";
_unit addHeadgear "rhsusf_mich_bare_norotos";
_unit addItemToUniform "ACE_EarPlugs";
_unit addItemToUniform "ACE_Flashlight_MX991";
for "_i" from 1 to 8 do {_unit addItemToUniform "ACE_elasticBandage";};
for "_i" from 1 to 8 do {_unit addItemToUniform "ACE_quikclot";};
for "_i" from 1 to 4 do {_unit addItemToUniform "ACE_tourniquet";};
for "_i" from 1 to 8 do {_unit addItemToUniform "ACE_packingBandage";};
for "_i" from 1 to 2 do {_unit addItemToUniform "Chemlight_green";};
for "_i" from 1 to 2 do {_unit addItemToUniform "Chemlight_blue";};

comment "VEST";
_unit addVest "rhsusf_spc_corpsman";
for "_i" from 1 to 10 do {_unit addItemToVest "rhs_mag_30Rnd_556x45_Mk318_Stanag";};
for "_i" from 1 to 8 do {_unit addItemToVest "rhs_mag_an_m8hc";};
for "_i" from 1 to 4 do {_unit addItemToVest "SmokeShellGreen";};

comment "BACKPACK";
_unit addBackpack "B_Carryall_cbr";
_unit addItemToBackpack "ACE_personalAidKit";
_unit addItemToBackpack "ACE_surgicalKit";
for "_i" from 1 to 30 do {_unit addItemToBackpack "ACE_elasticBandage";};
for "_i" from 1 to 30 do {_unit addItemToBackpack "ACE_quikclot";};
for "_i" from 1 to 5 do {_unit addItemToBackpack "ACE_morphine";};
for "_i" from 1 to 12 do {_unit addItemToBackpack "ACE_tourniquet";};
for "_i" from 1 to 5 do {_unit addItemToBackpack "ACE_salineIV_500";};
for "_i" from 1 to 30 do {_unit addItemToBackpack "ACE_packingBandage";};
for "_i" from 1 to 5 do {_unit addItemToBackpack "ACE_epinephrine";};

comment "WEAPONS";
_unit addWeapon "rhs_weap_m4_grip";
_unit addPrimaryWeaponItem "rhsusf_acc_ACOG2";
_unit addPrimaryWeaponItem "rhsusf_acc_anpeq15";
_unit addWeapon "Binocular";

comment "ITEMS";
_unit linkItem "ItemMap";
_unit linkItem "ItemCompass";
_unit linkItem "ItemWatch";
_unit linkItem "tf_anprc152_2";
_unit linkItem "ItemAndroid";
_unit linkItem "CUP_NVG_HMNVS";
