comment "25th MEU RTO and JTAC Desert";

private ["_unit"];
_unit = _this select 0;
IF(!local _unit) exitwith {};

comment "CLEAR";
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;

comment "UNIFORM";
_unit forceAddUniform "rhs_uniform_FROG01_d";
_unit addHeadgear "rhsusf_mich_helmet_marpatd";
_unit addItemToUniform "ACE_EarPlugs";
_unit addItemToUniform "ACE_Flashlight_MX991";
for "_i" from 1 to 8 do {_unit addItemToUniform "ACE_elasticBandage";};
for "_i" from 1 to 8 do {_unit addItemToUniform "ACE_quikclot";};
for "_i" from 1 to 4 do {_unit addItemToUniform "ACE_tourniquet";};
for "_i" from 1 to 8 do {_unit addItemToUniform "ACE_packingBandage";};
for "_i" from 1 to 4 do {_unit addItemToUniform "ACE_CableTie";};
for "_i" from 1 to 2 do {_unit addItemToUniform "Chemlight_green";};

comment "VEST";
_unit addVest "rhsusf_spc_squadleader";
_unit addItemToVest "rhsusf_mag_15Rnd_9x19_FMJ";
for "_i" from 1 to 4 do {_unit addItemToVest "rhs_mag_an_m8hc";};
for "_i" from 1 to 3 do {_unit addItemToVest "rhs_mag_m18_red";};
for "_i" from 1 to 3 do {_unit addItemToVest "SmokeShellGreen";};
for "_i" from 1 to 2 do {_unit addItemToVest "rhs_mag_m67";};
for "_i" from 1 to 11 do {_unit addItemToVest "rhs_mag_30Rnd_556x45_Mk318_Stanag";};

comment "BACKPACK";
_unit addBackpack "tf_rt1523g_sage";
_unit addItemToBackpack "ACE_EntrenchingTool";
_unit addItemToBackpack "Laserbatteries";
for "_i" from 1 to 3 do {_unit addItemToBackpack "rhs_mag_m662_red";};
for "_i" from 1 to 6 do {_unit addItemToBackpack "rhs_mag_m713_Red";};
for "_i" from 1 to 6 do {_unit addItemToBackpack "rhs_mag_m714_White";};

comment "WEAPONS";
_unit addWeapon "rhs_weap_m4_m203";
_unit addPrimaryWeaponItem "rhsusf_acc_ACOG2";
_unit addPrimaryWeaponItem "rhsusf_acc_anpeq15side";
_unit addWeapon "rhsusf_weap_m9";
_unit addWeapon "CUP_SOFLAM";

comment "ITEMS";
_unit linkItem "ItemMap";
_unit linkItem "ItemCompass";
_unit linkItem "ItemWatch";
_unit linkItem "tf_anprc152_1";
_unit linkItem "ItemcTab";
_unit linkItem "CUP_NVG_HMNVS";
