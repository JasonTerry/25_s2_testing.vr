comment "25th MEU Machinegunner Desert";

private ["_unit"];
_unit = _this select 0;
IF(!local _unit) exitwith {};

comment "Remove existing items";
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;

comment "UNIFORM";
_unit forceAddUniform "rhs_uniform_FROG01_d";
_unit addHeadgear "rhsusf_mich_helmet_marpatd";
_unit addItemToUniform "ACE_EarPlugs";
_unit addItemToUniform "ACE_Flashlight_MX991";
for "_i" from 1 to 8 do {_unit addItemToUniform "ACE_elasticBandage";};
for "_i" from 1 to 8 do {_unit addItemToUniform "ACE_quikclot";};
for "_i" from 1 to 4 do {_unit addItemToUniform "ACE_tourniquet";};
for "_i" from 1 to 8 do {_unit addItemToUniform "ACE_packingBandage";};
for "_i" from 1 to 4 do {_unit addItemToUniform "ACE_CableTie";};
for "_i" from 1 to 2 do {_unit addItemToUniform "Chemlight_green";};

comment "VEST";
_unit addVest "rhsusf_spc_mg";
for "_i" from 1 to 2 do {_unit addItemToVest "rhs_mag_an_m8hc";};
for "_i" from 1 to 2 do {_unit addItemToVest "rhsusf_mag_15Rnd_9x19_JHP";};
for "_i" from 1 to 2 do {_unit addItemToVest "rhsusf_100Rnd_762x51";};

comment "BACKPACK";
_unit addBackpack "rhsusf_assault_eagleaiii_coy";
_unit addItemToBackpack "ACE_SpareBarrel";
_unit addItemToBackpack "ACE_EntrenchingTool";
_unit addItemToBackpack "rhsusf_100Rnd_762x51_m61_ap";
_unit addItemToBackpack "rhsusf_100Rnd_762x51";

comment "WEAPONS";
_unit addWeapon "rhs_weap_m240B";
_unit addPrimaryWeaponItem"rhsusf_acc_ELCAN_ard";
_unit addWeapon "rhsusf_weap_m9";
_unit addWeapon "Binocular";

comment "ITEMS";
_unit linkItem "ItemMap";
_unit linkItem "ItemCompass";
_unit linkItem "ItemWatch";
_unit linkItem "tf_anprc152_2";
_unit linkItem "ItemAndroid";
_unit linkItem "CUP_NVG_HMNVS";
