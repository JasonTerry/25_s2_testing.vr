KEY -- ITEMS

"rhs_mag_an_m8hc"; // WHITE SMOKE
"SmokeShellRed"; // RED SMOKE
"SmokeShellGreen"; // GREEN SMOKE
"rhs_mag_m67"; // FRAG
"Chemlight_green"; // CHEM LIGHT GREEN

// KEY -- BASIC IFAK AND PERSONAL ITEMS
_unit addItemToUniform "ACE_EarPlugs";
_unit addItemToUniform "ACE_Flashlight_MX991";
for "_i" from 1 to 8 do {_unit addItemToUniform "ACE_elasticBandage";};
for "_i" from 1 to 8 do {_unit addItemToUniform "ACE_quikclot";};
for "_i" from 1 to 4 do {_unit addItemToUniform "ACE_tourniquet";};
for "_i" from 1 to 8 do {_unit addItemToUniform "ACE_packingBandage";};
for "_i" from 1 to 4 do {_unit addItemToUniform "ACE_CableTie";};

// KEY -- CLS IFAK AND PERSONAL ITEMS
_unit addItemToUniform "ACE_EarPlugs";
for "_i" from 1 to 8 do {_unit addItemToUniform "ACE_elasticBandage";};
for "_i" from 1 to 8 do {_unit addItemToUniform "ACE_quikclot";};
for "_i" from 1 to 4 do {_unit addItemToUniform "ACE_tourniquet";};
for "_i" from 1 to 8 do {_unit addItemToUniform "ACE_packingBandage";};
for "_i" from 1 to 2 do {_unit addItemToUniform "ACE_CableTie";};
for "_i" from 1 to 2 do {_unit addItemToUniform "ACE_morphine";};
for "_i" from 1 to 2 do {_unit addItemToUniform "ACE_epinephrine";};

_unit addItemToUniform "ACE_microDAGR"; // TL and above ONLY

// To add a LO to a unit within the mission, paste the following where THISLO.sqf = the name and extension of the LO
null = [this] execVM "LO\THISLO.sqf"; this addeventhandler ["respawn","_this execVM 'LO\THISLO.sqf'"];

// IF NEEDED FOR DEBUG USE DURING MISSION MAKING THIS IS HOW YOU ADD A VA TO A OBJECT!
// DO NOT EVER INCLUDE A VA IN A OFFIACL 25th MEU MISSION!!!!!!!!!
0 = ["AmmoboxInit",[this,true]] spawn BIS_fnc_arsenal;

--- VEHCLE LOADOUT SCRIPT TEMPLATE
comment "25thMEU JTF2 Operators Gear - vehicle Cargo Script";
private ["_veh"];
_veh = _this select 0;

comment "CLEAR";
clearBackpackCargoGlobal _veh;
clearItemCargoGlobal _veh;
clearWeaponCargoGlobal _veh;
clearMagazineCargoGlobal _veh;

comment "MAGAZINES CARGO";
_veh addMagazineCargoGlobal ["CUP_30Rnd_556x45_Stanag", 24];
_veh addMagazineCargoGlobal ["CUP_100Rnd_TE4_LRT4_Red_Tracer_762x51_Belt_M", 5];

comment "BACKPACK CARGO";
_veh addBackpackCargoGlobal ["tf_rt1523g_black",1];

comment "WEAPONS CARGO";
_veh addWeaponCargoGlobal ["CUP_arifle_M4A1_desert",5];
_veh addWeaponCargoGlobal ["CUP_lmg_Mk48_des",1];
_veh addWeaponCargoGlobal ["rhsusf_weap_glock17g4",6];
_veh addWeaponCargoGlobal ["ACE_Vector",1];

comment "ITEMS CARGO";
_veh addItemCargoGlobal ["CUP_HandGrenade_M67",6];

comment "FIRST AID"
_veh addItemCargoGlobal ["ACE_quikclot",20];
_veh addItemCargoGlobal ["ACE_morphine",10];
_veh addItemCargoGlobal ["ACE_tourniquet",12];
_veh addItemCargoGlobal ["ACE_salineIV_500",5];
_veh addItemCargoGlobal ["ACE_packingBandage",20];
_veh addItemCargoGlobal ["ACE_epinephrine",5];
_veh addItemCargoGlobal ["ACE_personalAidKit",1];
_veh addItemCargoGlobal ["ACE_surgicalKit",1];---
