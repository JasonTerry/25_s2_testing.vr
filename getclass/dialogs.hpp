
class ClassnameDefiner
{
	idd=-1;
	movingenable=false;

	class controls
	{

		class classname_helmet: RscButton
		{
			idc = 1600;
			text = "Helmet"; //--- ToDo: Localize;
			x = 0.319531 * safezoneW + safezoneX;
			y = 0.2734 * safezoneH + safezoneY;
			w = 0.0721875 * safezoneW;
			h = 0.055 * safezoneH;
			action = "_nil = [] execVM ""getclass\helmet.sqf""";
		};
		class classname_uniform: RscButton
		{
			idc = 1601;
			text = "Uniform"; //--- ToDo: Localize;
			x = 0.319531 * safezoneW + safezoneX;
			y = 0.3306 * safezoneH + safezoneY;
			w = 0.0721875 * safezoneW;
			h = 0.055 * safezoneH;
			action = "_nil = [] execVM ""getclass\uniform.sqf""";
		};
		class classname_vest: RscButton
		{
			idc = 1602;
			text = "Vest"; //--- ToDo: Localize;
			x = 0.319531 * safezoneW + safezoneX;
			y = 0.3878 * safezoneH + safezoneY;
			w = 0.0721875 * safezoneW;
			h = 0.055 * safezoneH;
			action = "_nil = [] execVM ""getclass\vest.sqf""";
		};
		class classname_backpack: RscButton
		{
			idc = 1603;
			text = "Backpack"; //--- ToDo: Localize;
			x = 0.319531 * safezoneW + safezoneX;
			y = 0.445 * safezoneH + safezoneY;
			w = 0.0721875 * safezoneW;
			h = 0.055 * safezoneH;
			action = "_nil = [] execVM ""getclass\backpack.sqf""";
		};
		class classname_primWeap: RscButton
		{
			idc = 1604;
			text = "Primary Weapon"; //--- ToDo: Localize;
			x = 0.319531 * safezoneW + safezoneX;
			y = 0.5462 * safezoneH + safezoneY;
			w = 0.0979687 * safezoneW;
			h = 0.055 * safezoneH;
			action = "_nil = [] execVM ""getclass\primaryWeapon.sqf""";
		};
		class classname_secWeap: RscButton
		{
			idc = 1605;
			text = "Secondary Weapon"; //--- ToDo: Localize;
			x = 0.451532 * safezoneW + safezoneX;
			y = 0.5462 * safezoneH + safezoneY;
			w = 0.0979687 * safezoneW;
			h = 0.055 * safezoneH;
			action = "_nil = [] execVM ""getclass\secondaryWeapon.sqf""";
		};
		class classname_handgun: RscButton
		{
			idc = 1606;
			text = "Handgun"; //--- ToDo: Localize;
			x = 0.5825 * safezoneW + safezoneX;
			y = 0.5462 * safezoneH + safezoneY;
			w = 0.0979687 * safezoneW;
			h = 0.055 * safezoneH;
			action = "_nil = [] execVM ""getclass\handgunWeapon.sqf""";
		};
		class classname_glasses: RscButton
		{
			idc = 1607;
			text = "Glasses"; //--- ToDo: Localize;
			x = 0.402031 * safezoneW + safezoneX;
			y = 0.2734 * safezoneH + safezoneY;
			w = 0.0721875 * safezoneW;
			h = 0.055 * safezoneH;
			action = "_nil = [] execVM ""getclass\glasses.sqf""";
		};
		class classname_nightVision: RscButton
		{
			idc = 1608;
			text = "Night Vision"; //--- ToDo: Localize;
			x = 0.402031 * safezoneW + safezoneX;
			y = 0.3306 * safezoneH + safezoneY;
			w = 0.0721875 * safezoneW;
			h = 0.055 * safezoneH;
			action = "_nil = [] execVM ""getclass\nightVision.sqf""";
		};
		class classname_binocular: RscButton
		{
			idc = 1609;
			text = "Binocular"; //--- ToDo: Localize;
			x = 0.402031 * safezoneW + safezoneX;
			y = 0.3878 * safezoneH + safezoneY;
			w = 0.0721875 * safezoneW;
			h = 0.055 * safezoneH;
			action = "_nil = [] execVM ""getclass\binocular.sqf""";
		};
		class classname_primWeapSil: RscButton
		{
			idc = 1610;
			text = "Silencer"; //--- ToDo: Localize;
			x = 0.319531 * safezoneW + safezoneX;
			y = 0.6122 * safezoneH + safezoneY;
			w = 0.0670312 * safezoneW;
			h = 0.022 * safezoneH;
			action = "_nil = [] execVM ""getclass\primaryWeaponSilencer.sqf""";
		};
		class classname_primWeapLaser: RscButton
		{
			idc = 1611;
			text = "Laserpointer"; //--- ToDo: Localize;
			x = 0.319531 * safezoneW + safezoneX;
			y = 0.6364 * safezoneH + safezoneY;
			w = 0.0670312 * safezoneW;
			h = 0.022 * safezoneH;
			action = "_nil = [] execVM ""getclass\primaryWeaponLaserpointer.sqf""";
		};
		class classname_primWeapScope: RscButton
		{
			idc = 1612;
			text = "Scope"; //--- ToDo: Localize;
			x = 0.319531 * safezoneW + safezoneX;
			y = 0.6606 * safezoneH + safezoneY;
			w = 0.0670312 * safezoneW;
			h = 0.022 * safezoneH;
			action = "_nil = [] execVM ""getclass\primaryWeaponScope.sqf""";
		};
		class classname_primWeapMag: RscButton
		{
			idc = 1613;
			text = "Magazine"; //--- ToDo: Localize;
			x = 0.319531 * safezoneW + safezoneX;
			y = 0.6848 * safezoneH + safezoneY;
			w = 0.0670312 * safezoneW;
			h = 0.022 * safezoneH;
			action = "_nil = [] execVM ""getclass\primaryWeaponMagazine.sqf""";
		};
		class classname_secWeapSil: RscButton
		{
			idc = 1614;
			text = "Silencer"; //--- ToDo: Localize;
			x = 0.453594 * safezoneW + safezoneX;
			y = 0.6122 * safezoneH + safezoneY;
			w = 0.0670312 * safezoneW;
			h = 0.022 * safezoneH;
			action = "_nil = [] execVM ""getclass\secondaryWeaponSilencer.sqf""";
		};
		class classname_secWeapLaser: RscButton
		{
			idc = 1615;
			text = "Laserpointer"; //--- ToDo: Localize;
			x = 0.453594 * safezoneW + safezoneX;
			y = 0.6364 * safezoneH + safezoneY;
			w = 0.0670312 * safezoneW;
			h = 0.022 * safezoneH;
			action = "_nil = [] execVM ""getclass\secondaryWeaponLaserpointer.sqf""";
		};
		class classname_secWeapScope: RscButton
		{
			idc = 1616;
			text = "Scope"; //--- ToDo: Localize;
			x = 0.453594 * safezoneW + safezoneX;
			y = 0.6606 * safezoneH + safezoneY;
			w = 0.0670312 * safezoneW;
			h = 0.022 * safezoneH;
			action = "_nil = [] execVM ""getclass\secondaryWeaponScope.sqf""";
		};
		class classname_secWeapMag: RscButton
		{
			idc = 1617;
			text = "Magazine"; //--- ToDo: Localize;
			x = 0.453594 * safezoneW + safezoneX;
			y = 0.6848 * safezoneH + safezoneY;
			w = 0.0670312 * safezoneW;
			h = 0.022 * safezoneH;
			action = "_nil = [] execVM ""getclass\secondaryWeaponMagazine.sqf""";
		};
		class classname_handgunSil: RscButton
		{
			idc = 1618;
			text = "Silencer"; //--- ToDo: Localize;
			x = 0.582485 * safezoneW + safezoneX;
			y = 0.6122 * safezoneH + safezoneY;
			w = 0.0670312 * safezoneW;
			h = 0.022 * safezoneH;
			action = "_nil = [] execVM ""getclass\handgunWeaponSilencer.sqf""";
		};
		class classname_handgunLaser: RscButton
		{
			idc = 1619;
			text = "Laserpointer"; //--- ToDo: Localize;
			x = 0.5825 * safezoneW + safezoneX;
			y = 0.6364 * safezoneH + safezoneY;
			w = 0.0670312 * safezoneW;
			h = 0.022 * safezoneH;
			action = "_nil = [] execVM ""getclass\handgunWeaponLaserpointer.sqf""";
		};
		class classname_handgunScope: RscButton
		{
			idc = 1620;
			text = "Scope"; //--- ToDo: Localize;
			x = 0.5825 * safezoneW + safezoneX;
			y = 0.6606 * safezoneH + safezoneY;
			w = 0.0670312 * safezoneW;
			h = 0.022 * safezoneH;
			action = "_nil = [] execVM ""getclass\handgunWeaponScope.sqf""";
		};
		class classname_handgunMag: RscButton
		{
			idc = 1621;
			text = "Magazine"; //--- ToDo: Localize;
			x = 0.5825 * safezoneW + safezoneX;
			y = 0.6848 * safezoneH + safezoneY;
			w = 0.0670312 * safezoneW;
			h = 0.022 * safezoneH;
			action = "_nil = [] execVM ""getclass\handgunWeaponMagazine.sqf""";
		};
		class classname_vehicle: RscButton
		{
			idc = 1622;
			text = "Vehicle"; //--- ToDo: Localize;
			x = 0.530937 * safezoneW + safezoneX;
			y = 0.2734 * safezoneH + safezoneY;
			w = 0.0721875 * safezoneW;
			h = 0.055 * safezoneH;
			action = "_nil = [] execVM ""getclass\vehicle.sqf""";
		};
		class classname_cursortarget: RscButton
		{
			idc = 1623;
			text = "Cursor target"; //--- ToDo: Localize;
			x = 0.530937 * safezoneW + safezoneX;
			y = 0.3306 * safezoneH + safezoneY;
			w = 0.0721875 * safezoneW;
			h = 0.055 * safezoneH;
			action = "_nil = [] execVM ""getclass\cursorTarget.sqf""";
		};
		class classname_text: RscText
		{
			idc = 1000;
			text = "Classnamer by Paolo Camorra"; //--- ToDo: Localize;
			x = 0.292719 * safezoneW + safezoneX;
			y = 0.7354 * safezoneH + safezoneY;
			w = 0.0876563 * safezoneW;
			h = 0.055 * safezoneH;
			sizeEx = 0.04300;
		};
		class classname_environment: RscText
		{
			idc = 1002;
			text = "Environment"; //--- ToDo: Localize;
			x = 0.530937 * safezoneW + safezoneX;
			y = 0.1964 * safezoneH + safezoneY;
			w = 0.175313 * safezoneW;
			h = 0.055 * safezoneH;
			sizeEx = 0.12300;
		};
		class classname_player: RscText
		{
			idc = 1001;
			text = "Player"; //--- ToDo: Localize;
			x = 0.353563 * safezoneW + safezoneX;
			y = 0.192 * safezoneH + safezoneY;
			w = 0.0928125 * safezoneW;
			h = 0.066 * safezoneH;
			sizeEx = 0.12300;
		};


		class classname_close: RscButton
		{
			idc = 1622;
			text = "Close"; //--- ToDo: Localize;
			x = 0.675313 * safezoneW + safezoneX;
			y = 0.742 * safezoneH + safezoneY;
			w = 0.0309375 * safezoneW;
			h = 0.033 * safezoneH;
			colorBackground[] = {1, 0, 0, 1};
			action = closeDialog 0;
		};
		class classname_copymode: RscButton
		{
			idc = 1623;
			text = "Copy Mode"; //--- ToDo: Localize;
			x = 0.62375 * safezoneW + safezoneX;
			y = 0.742 * safezoneH + safezoneY;
			w = 0.0515625 * safezoneW;
			h = 0.033 * safezoneH;
			action = "_nil = [] execVM ""getclass\copyModeInfo.sqf"""
		};

};
};
