// 25th MEU Opfor Simple SQF Script

private ["_25thgroup"];
private ["_25thunits"];
private ["_i"];
private ["_cnt", "_cnt1","_cnt2","_cnt3","_cnt4","_cnt5","_cnt6"];
private ["_qrf_sent"];

_25thgroup = group ripper_actual;
_25thunits = units _25thgroup;
_qrf_sent = false;
// GET OPFOR COUNT
// IF OPFOR COUNT IS OPFOR COUNT - 10
// QRF 1
// ESCALATE
_cnt1 = _cnt - 5;
_cnt2 = _cnt - 10;
_cnt3 = _cnt - 15;
_cnt4 = _cnt - 20;
_cnt5 = _cnt - 25;
_cnt6 = _cnt - 30;

hint format ["running qrf"];

while {!_qrf_sent} do {
	_cnt = {(side _x) == east} count allUnits)}; // Get _cnt
  hint format ["TOTAL CURRENT = %1", _cnt  ];
	if(_cnt <= _cnt1) then { // qf1 sets out.
    hint format ["qrf 1 inbound"];
    sleep 1;
    while {(count (waypoints group qf1)) > 0} do {
    deleteWaypoint ((waypoints group qf1) select 0);
    };
    sleep 1;
    waypoint1 = (group qf1) addwaypoint [getpos ripper_actual,0];
	};
  if(_cnt <= _cnt2) then { // qf2 sets out.
    hint format ["qrf 2 inbound"];
    sleep 1;
    while {(count (waypoints group qf2)) > 0} do {
    deleteWaypoint ((waypoints group qf2) select 0);
    };
    sleep 1;
    waypoint1 = (group qf2) addwaypoint [getpos ripper_actual,0];
  };
  if(_cnt <= _cnt3) then { // qf3 sets out.
    hint format ["qrf 3 inbound"];
    sleep 1;
    while {(count (waypoints group qf3)) > 0} do {
    deleteWaypoint ((waypoints group qf3) select 0);
    };
    sleep 1;
    waypoint1 = (group qf3) addwaypoint [getpos ripper_actual,0];
  };
  if(_cnt <= _cnt4 ) then { // qf4 sets out.
    hint format ["qrf 4 inbound"];
    sleep 1;
    while {(count (waypoints group qf4)) > 0} do {
    deleteWaypoint ((waypoints group qf4) select 0);
    };
    sleep 1;
    waypoint1 = (group qf4) addwaypoint [getpos ripper_actual,0];
  };
  if(_cnt <= _cnt5) then { // qf5 sets out.
    hint format ["qrf 5 inbound"];
    sleep 1;
    while {(count (waypoints group qf5)) > 0} do {
    deleteWaypoint ((waypoints group qf5) select 0);
    };
    sleep 1;
    waypoint1 = (group qf5) addwaypoint [getpos ripper_actual,0];
  };
  if(_cnt <= _cnt6) then { // qf6 sets out.
    hint format ["qrf 5 inbound"];
    sleep 1;
    while {(count (waypoints group qf6)) > 0} do {
    deleteWaypoint ((waypoints group qf6) select 0);
    };
    sleep 1;
    waypoint1 = (group qf6) addwaypoint [getpos ripper_actual,0];
    _qrf_sent = true;
  };
	sleep 60; // Check every minute
};
